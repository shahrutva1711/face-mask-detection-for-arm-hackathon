This model is for detecting the mask on the face, which has bceome a compulsion nowadays due to the spread of the novel coronavirus. The world stopped since a year, but can't be stopped more, so for the offices to get start with all the staff is necessary, and keeping in mind the safety of everyone wearing a mask is inevitable. 

The model uses MobileNEtv2 of Convolutional Neural Network for detecting mask without need of any extrenal hardware, using the web cam of the laptop itself.

Flow of the program :
1) Firstly we import the necessary libraries of tensorflow, keras, opencv2 etc.
2) The train_mask_detector.py file is run with the mask_model file, which trains the model on a dataset with almost 3500 images of with mask and without mask, this also gives the plot of the accuracy which is also uploaded.
3) Then the detect_mask_video.py is run, which opens a live streaming video which gives a green bounding with accuracy if the person is wearing a mask properly, and if not, then it gives a red bounding box with its accuracy.

We tried the model with three different values for the hyperparameters and compared them and chose the one with the best accuracy of 99%. We have also tested the model from different distances from the webcam. And the live stream and the ppt will be shown on the presentation day.

Future work : We will try and connect it with hardware, and messaging system, such as it can be used ofr attendanc purpose in offices, and can send AI alerts with the image of the person captured to the system if anybody is entering without a mask.

Team Members :
1) Rutva Jignesh Shah
2) Kartikkumar Patel
3) Hari Krishna Katta
4) Shreyas Pawar
5) Sandeep Mishra








